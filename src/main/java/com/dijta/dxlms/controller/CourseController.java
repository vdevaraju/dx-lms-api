package com.dijta.dxlms.controller;

import com.dijta.common.multitenant.BaseMultiTenantEntityController;
import com.dijta.common.security.ISecurityContext;
import com.dijta.dxlms.constant.ApplicationConstants;
import com.dijta.dxlms.dto.CourseDto;
import com.dijta.dxlms.entity.Course;
import com.dijta.dxlms.service.CourseService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/course")
public class CourseController extends BaseMultiTenantEntityController<CourseDto, Course> {

    protected CourseController(CourseService courseService,
                               ISecurityContext securityContext) {
        super(courseService, securityContext, ApplicationConstants.lmsBasePrivilege());
    }
}
