package com.dijta.dxlms.controller;

import com.dijta.common.multitenant.BaseMultiTenantEntityController;
import com.dijta.common.security.ISecurityContext;
import com.dijta.dxlms.constant.ApplicationConstants;
import com.dijta.dxlms.dto.ModuleDto;
import com.dijta.dxlms.entity.Module;
import com.dijta.dxlms.service.ModuleService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/module")
public class ModuleController extends BaseMultiTenantEntityController<ModuleDto, Module> {

    protected ModuleController(ModuleService moduleService,
                               ISecurityContext securityContext) {
        super(moduleService, securityContext, ApplicationConstants.lmsBasePrivilege());
    }
}
