package com.dijta.dxlms.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "section")
@DiscriminatorValue("section")
@Getter
@Setter
public class Section extends DxModule {

    public Section() {
        super("section");
    }

    @OneToOne
    @JoinColumn(name = "section_type", referencedColumnName = "pk_id")
    private SectionType sectionType;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "content_position")
    private String contentPosition;

    @ManyToOne
    @JoinColumn(name = "module_id", referencedColumnName = "pk_id")
    private Module module;
}
