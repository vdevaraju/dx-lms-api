package com.dijta.dxlms.entity;

import com.dijta.common.multitenant.MultiTenantEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "section_page_content")
public class SectionPageContent extends MultiTenantEntity {

    @ManyToOne
    @JoinColumn(name = "section_id", referencedColumnName = "pk_id")
    private Section section;

    @Column(columnDefinition = "varchar(max)", length = 8000)
    private String pageContent;
}
