package com.dijta.dxlms.entity;

import com.dijta.common.multitenant.MultiTenantEntity;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "question_type")
public class QuestionType extends MultiTenantEntity {

    private String questionType;
}
