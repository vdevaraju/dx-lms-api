package com.dijta.dxlms.entity;

import com.dijta.common.multitenant.MultiTenantEntity;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "dx_module")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "table_name",
        discriminatorType = DiscriminatorType.STRING)
public class DxModule extends MultiTenantEntity {

    @Column(name = "module_name", nullable = false)
    private String moduleName;

    public DxModule(final String moduleName) {
        this.moduleName = moduleName;
    }
}
