package com.dijta.dxlms.entity;

import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "course")
@DiscriminatorValue("course")
@Getter
@Setter
public class Course extends DxModule {

    private String courseName;

    private String type;

    public Course() {
        super("course");
    }

    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY)
    private List<Module> modules;
}
