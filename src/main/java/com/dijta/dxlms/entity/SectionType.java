package com.dijta.dxlms.entity;

import com.dijta.common.multitenant.MultiTenantEntity;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "SectionType")
@Getter
@Setter
public class SectionType extends MultiTenantEntity {

    private String sectionType;
}
