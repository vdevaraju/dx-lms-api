package com.dijta.dxlms.entity;

import com.dijta.common.multitenant.MultiTenantEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "question_options")
public class QuestionOptions extends MultiTenantEntity {

    @ManyToOne
    @JoinColumn(name = "question_id", referencedColumnName = "pk_id")
    private Question question;

    @Column(name = "option_value")
    private String optionValue;

    @Column(name = "is_answer")
    private Boolean isAnswer;
}
