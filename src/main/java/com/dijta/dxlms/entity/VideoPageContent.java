package com.dijta.dxlms.entity;

import com.dijta.common.multitenant.MultiTenantEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "video_page_content")
public class VideoPageContent extends MultiTenantEntity {

    @ManyToOne
    @JoinColumn(name = "section_id", referencedColumnName = "pk_id")
    private Section section;

    @Column(name = "video_content_url", length = 1024)
    private String videoContentUrl;
}
