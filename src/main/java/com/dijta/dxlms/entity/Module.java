package com.dijta.dxlms.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "module")
@DiscriminatorValue("module")
@Getter
@Setter
public class Module extends DxModule {

    private String title;

    private String description;

    public Module() {
        super("course");
    }

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id", referencedColumnName = "pk_id")
    private Course course;

}
