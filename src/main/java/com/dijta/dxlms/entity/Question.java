package com.dijta.dxlms.entity;

import com.dijta.common.multitenant.MultiTenantEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "question")
public class Question extends MultiTenantEntity {

    @ManyToOne
    @JoinColumn(name = "quiz_bank_id", referencedColumnName = "pk_id")
    private QuizBank quizBank;

    @ManyToOne
    @JoinColumn(name = "question_type_id", referencedColumnName = "pk_id")
    private QuestionType questionType;

    @Column(name = "question_text")
    private String questionText;

    @Column(name = "question_points")
    private Long questionPoints;

    @Column(name = "answer_randomized")
    private Boolean answerRandomized;

    @Column(name = "is_show_answer")
    private Boolean isShowAnswer;
}
