package com.dijta.dxlms.entity;

import com.dijta.common.multitenant.MultiTenantEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "quiz_bank")
public class QuizBank extends MultiTenantEntity {


    @ManyToOne
    @JoinColumn(name = "assessment_id", referencedColumnName = "pk_id")
    private Assessment assessment;

    @Column(name = "quiz_bank_title")
    private String quizBankTitle;
}
