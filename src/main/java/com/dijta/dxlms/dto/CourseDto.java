package com.dijta.dxlms.dto;

import com.dijta.common.multitenant.MulitTenantDTO;
import com.dijta.dxlms.entity.Module;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourseDto extends MulitTenantDTO {

    private static final long serialVersionUID = 6190055956373926168L;

    private String courseName;

    private String type;

    private List<Module> modules;
}
