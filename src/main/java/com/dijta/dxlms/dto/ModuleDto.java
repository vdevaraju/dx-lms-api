package com.dijta.dxlms.dto;

import com.dijta.common.multitenant.MulitTenantDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModuleDto extends MulitTenantDTO {

    private static final long serialVersionUID = -7430017636187957083L;

    private String title;

    private String description;

    private Long courseId;

    private CourseDto course;

    /**
     * Set Course ID for the given module
     * @param courseId : courseId to which this module associated
     */
    public void setCourseId(final Long courseId) {
        this.courseId = courseId;
        this.course = new CourseDto();
        this.course.setPkId(courseId);
    }
}
