package com.dijta.dxlms.constant;

import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpMethod;


public class ApplicationConstants {

    /**
     * Method which will have the privielge for the controllers
     * @return : Map with privilges for each HttpMethod Type.
     */

    public static Map<HttpMethod, String> lmsBasePrivilege() {
        final HashMap<HttpMethod, String> map = new HashMap<>();
        map.put(HttpMethod.GET, "hasAnyAuthority('ROLE_USER')");
        map.put(HttpMethod.POST, "hasAnyAuthority('ROLE_USER')");
        map.put(HttpMethod.PUT, "hasAnyAuthority('ROLE_USER')");
        map.put(HttpMethod.DELETE, "hasAnyAuthority('ROLE_USER')");
        return map;
    }
}
