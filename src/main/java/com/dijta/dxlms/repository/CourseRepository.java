package com.dijta.dxlms.repository;

import com.dijta.common.multitenant.MultiTenantEntityRepository;
import com.dijta.dxlms.entity.Course;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends MultiTenantEntityRepository<Course> {
}
