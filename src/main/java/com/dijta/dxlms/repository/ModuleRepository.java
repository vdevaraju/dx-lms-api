package com.dijta.dxlms.repository;

import com.dijta.common.multitenant.MultiTenantEntityRepository;
import com.dijta.dxlms.entity.Module;
import org.springframework.stereotype.Repository;

@Repository
public interface ModuleRepository extends MultiTenantEntityRepository<Module> {
}
