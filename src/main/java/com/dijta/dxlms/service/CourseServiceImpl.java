package com.dijta.dxlms.service;

import com.dijta.common.identity.DijtaConverters;
import com.dijta.common.multitenant.AbstractMultiTenantEntityService;
import com.dijta.common.security.ISecurityContext;
import com.dijta.dxlms.dto.CourseDto;
import com.dijta.dxlms.entity.Course;
import com.dijta.dxlms.repository.CourseRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl extends AbstractMultiTenantEntityService<CourseDto, Course>
        implements CourseService {


    protected CourseServiceImpl(CourseRepository courseRepository,
                                DozerBeanMapper dozerBeanMapper,
                                ISecurityContext securityContext) {
        super(courseRepository, DijtaConverters.converter(dozerBeanMapper, CourseDto.class, Course.class),
                securityContext);
    }

    @Override
    protected Course getEntityForExample() {
        return new Course();
    }
}
