package com.dijta.dxlms.service;

import com.dijta.common.identity.DijtaConverters;
import com.dijta.common.multitenant.AbstractMultiTenantEntityService;
import com.dijta.common.security.ISecurityContext;
import com.dijta.dxlms.dto.ModuleDto;
import com.dijta.dxlms.entity.Module;
import com.dijta.dxlms.repository.ModuleRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

@Service
public class ModuleServiceImpl extends AbstractMultiTenantEntityService<ModuleDto, Module>
        implements ModuleService {

    protected ModuleServiceImpl(ModuleRepository moduleRepository,
                                DozerBeanMapper dozerBeanMapper,
                                ISecurityContext securityContext) {
        super(moduleRepository, DijtaConverters.converter(dozerBeanMapper, ModuleDto.class, Module.class),
                securityContext);
    }

    @Override
    protected Module getEntityForExample() {
        return new Module();
    }
}
