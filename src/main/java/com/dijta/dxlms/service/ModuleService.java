package com.dijta.dxlms.service;

import com.dijta.common.multitenant.MultiTenantEntityService;
import com.dijta.dxlms.dto.ModuleDto;
import com.dijta.dxlms.entity.Module;

public interface ModuleService extends MultiTenantEntityService<ModuleDto, Module> {
}
