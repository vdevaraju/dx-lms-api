package com.dijta.dxlms.service;

import com.dijta.common.multitenant.MultiTenantEntityService;
import com.dijta.dxlms.dto.CourseDto;
import com.dijta.dxlms.entity.Course;

public interface CourseService extends MultiTenantEntityService<CourseDto, Course> {
}
