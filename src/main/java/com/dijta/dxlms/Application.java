package com.dijta.dxlms;

import com.dijta.common.EnableDijtaBase;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDijtaBase
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
